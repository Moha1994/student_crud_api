<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
include_once 'db.php';
include_once 'student.php';
$database = new Database();
$db = $database->getConnection();
$student = new Student($db);
$data = json_decode(file_get_contents("php://input"));
if(
    !empty($data->std_id) && !empty($data->std_name) && !empty($data->std_age) && !empty($data->class) && !empty($data->date_of_enrollment))
{
	$student->std_id=$data->std_id;
	$student->std_name = $data->std_name;
	$student->std_age = $data->std_age;
    $student->class= $data->class;
    $student->date_of_enrollment = $data->date_of_enrollment;
 if($student->create_student())
 {
     	http_response_code(201);
 		echo json_encode(array("message" => "Student was created."));
 }
 else{
 
	http_response_code(503);
	echo json_encode(array("message" => "Unable to create Student."));
 }

}
?>
