<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
 
// include database and object files
include_once 'db.php';
include_once 'student.php';
$database = new Database();
$db = $database->getConnection();
$student = new Student($db);
$stmt = $student->read_student();
$num = $stmt->rowCount();
if($num>=0){
    $student_arr=array();
    $student_arr["records"]=array();
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){ extract($row);
        $student_item=array(
           "std_id" => $std_id,
            "std_name" => $std_name,
            "std_age" => $std_age,
            "class" => $class,
            "date_of_enrollment"=>$date_of_enrollment
        );
 
        array_push($student_arr["records"], $student_item);
    }
    http_response_code(200);
    echo json_encode($student_arr);
    array("message" => "Student in our database.");
}
 
else
{
    http_response_code(404);
    echo json_encode($student_arr);
    array("message" => "No Student in our database.");
}