<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: GET");
header("Access-Control-Allow-Credentials: true");
header('Content-Type: application/json');
include_once 'db.php';
include_once 'student.php';
$database = new Database();
$db = $database->getConnection();
$student = new Student($db);
 
$student->std_id = isset($_GET['std_id']) ? $_GET['std_id'] : die();
 
$student->readOne_student();
 
if($student->name!=null){
    // create array
    $student_arr = array(
            "std_id" => $std_id,
            "std_name" => $std_name,
            "std_age" => $std_age,
            "class" => $class,
            "date_of_enrollment"=>$date_of_enrollment
    );

    http_response_code(200);
    echo json_encode($student_arr);
}
 
else{
    http_response_code(404);
    echo json_encode(array("message" => "Student does not exist."));
}
?>