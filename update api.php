<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
 
// include database and object files
include_once 'db.php';
include_once 'student.php';
 
// get database connection
$database = new Database();
$db = $database->getConnection();
 
// prepare student object
$student = new Student($db);
$data = json_decode(file_get_contents("php://input"));

	$student->std_id=$data->std_id;
	$student->std_name = $data->std_name;
	$student->std_age = $data->std_age;
    $student->class= $data->class;
    $student->date_of_enrollment = $data->date_of_enrollment;
 
// update the student
if($student->update_student()){

    http_response_code(200);
    echo json_encode(array("message" => "Student was updated."));
}
 
else{
    http_response_code(503);
    echo json_encode(array("message" => "Unable to update student."));
}
?>