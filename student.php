<?php
class Student{
 
    // database connection and table name
    private $conn;
    private $table_name = "student";
 
    // object properties
    public $std_id;
    public $std_name;
    public $std_age;
    public $class;
    public $date_of_enrollment;
 

    // constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
    }

function create_student()
	{

		  $query = "INSERT INTO
                " . $this->table_name . " SET std_id=:std_id, std_name=:std_name, std_age=:std_age,class=:class,date_of_enrollment=:date_of_enrollment";
		$stmt = $this->conn->prepare($query);

    $this->std_id=htmlspecialchars(strip_tags($this->std_id));
    $this->std_name=htmlspecialchars(strip_tags($this->std_name));
    $this->std_age=htmlspecialchars(strip_tags($this->std_age));
     $this->class=htmlspecialchars(strip_tags($this->class));
      $this->date_of_enrollment=htmlspecialchars(strip_tags($this->date_of_enrollment));
 
    // bind values
    $stmt->bindParam(":std_id", $this->std_id);
    $stmt->bindParam(":std_name", $this->std_name);
    $stmt->bindParam(":std_age", $this->std_age);
    $stmt->bindParam(":class", $this->class);
    $stmt->bindParam(":date_of_enrollment", $this->date_of_enrollment);
    // execute query
    if($stmt->execute()){
        return true;
    }

    return false;
	}
function read_student(){
		$query="SELECT * FROM student";
		 // prepare query statement
    $stmt = $this->conn->prepare($query);
 
    // execute query
    $stmt->execute();
 
    return $stmt;
	}
function update_student(){
		  $query = "UPDATE " . $this->table_name . "  SET  std_id=:std_id, std_name=:std_name, std_age=:std_age,class:class,date_of_enrollment:date_of_enrollment WHERE std_id = :std_id";
		 $stmt = $this->conn->prepare($query);

	$this->std_id=htmlspecialchars(strip_tags($this->std_id));
    $this->std_name=htmlspecialchars(strip_tags($this->std_name));
    $this->std_age=htmlspecialchars(strip_tags($this->std_age));
     $this->class=htmlspecialchars(strip_tags($this->class));
      $this->date_of_enrollment=htmlspecialchars(strip_tags($this->date_of_enrollment));
 
    // bind values
    $stmt->bindParam(":std_id", $this->std_id);
    $stmt->bindParam(":std_name", $this->std_name);
    $stmt->bindParam(":std_age", $this->std_age);
    $stmt->bindParam(":class", $this->class);
    $stmt->bindParam(":date_of_enrollment", $this->date_of_enrollment);
	// execute the query
    if($stmt->execute()){
        return true;
    }
 
    return false;
	}
function delete_student()
{
	// delete query
    $query = "DELETE FROM " . $this->table_name . " WHERE std_id= ?";
 
    // prepare query
    $stmt = $this->conn->prepare($query);
 
    // sanitize
    $this->std_id=htmlspecialchars(strip_tags($this->std_id));
 
    // bind id of record to delete
    $stmt->bindParam(1, $this->std_id);
 
    // execute query
    if($stmt->execute()){
        return true;
    }
 
    return false;

}
function readOne_student(){
 
    // query to read single record
    $query = "SELECT
                std_id as std_id, std_id,std_name, std_age, class, date_of_enrollment
            FROM student";
 
    // prepare query statement
    $stmt = $this->conn->prepare( $query );
 
    // bind id of product to be updated
    $stmt->bindParam(1, $this->std_id);
 
    // execute query
    $stmt->execute();
 
    // get retrieved row
    $row = $stmt->fetch(PDO::FETCH_ASSOC);
 
    // set values to object properties
    $this->std_id = $row['std_id'];
    $this->std_name = $row['std_name'];
    $this->std_age = $row['std_age'];
    $this->class= $row['class'];
    $this->date_of_enrollment = $row['date_of_enrollment'];
}

}
